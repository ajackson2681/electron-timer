# ElectronSplit

## License
This project uses a GPLv3 license. For more information, see LICENSE.md.

## Overview
This is meant to be a simple to use stopwatch timer written in plain JavaScript and bundled as an electron app. The primary purpose was to be used as a speedrun timer, but can easily be used for other purposes that require a timer.

## Requirements for Building
You will only require Node.js version 12.0.0+ to build this project, as the rest is handled by Node Package Manager. This could potentially be build on older versions of Node.js, but has only been tested and supported on v12 or later. 

## Build Process
The build process is very simple. Simply clone the repo, navigate to the root directory and run
```
npm install  
```
And you're ready to go! To launch the project, execute:
```
npm start
```
Below are the other scripts provided in the package.json and what they do:  

* npm run clean - deletes the node_modules and release-builds directories  
* npm run package-linux - Packages the app in asar format for linux systems  
* npm run make-deb - Creates the .deb file for Debian-based linux distros  
* npm run build-debian - Runs the above two commands in succession  

## Installation
NOTE: There is no current plan to support Windows, due to my inexperience with developing electron-based applications on that platform.

For Debian-based linux distros, download the latest .deb file, and open a terminal in the folder to which it downloaded and execute the following command:  
```
sudo apt install electron-timer_*_amd64.deb
```

For other non-debian based linux users, you will want to download the electron-timer_<version>_amd64.zip. Navigate to the folder it was downloaded to and open a terminal and excute the following commands (replacing <version\> with the correct version number):  
```
unzip electron-timer_<version>_.zip
cd electron-timer_<version>_amd64
chmod +x install.sh
sudo ./install.sh
```
The timer can then be run by just executing the following command in the terminal:
```
electron-timer
```

NOTE: This installation will also work on Debian-based distros, but it is recommended to use the .deb package instead. Also, you _need_ to run the install.sh script using root privilages, as it places the source files in the /lib/ directory and creates a symlink in your /usr/bin directory. If you are uncomfortable with doing this, you can always leave the electron-timer-linux-x64 directory wherever you want, and run it by executing the following command:
```
<path-where-source-is-located>/electron-timer-linux-x64/electron-timer
```

## How to use
The timer is ready to use right out of the box. By default, to start or pause the timer, simply use the key combination Ctrl+\`. To reset the timer, the default combination is Ctrl+Alt+\`.

## Configuring
To customize the timer, there will be a small amount of knowledge of CSS involved. However, it's very minimal.You will need to edit the configuration file named electron-timer.json in the following directory:

`/home/<username>/.config/`

the default config can be found below:

```
{
    "styles": {
        "background-color": "magenta",
        "color:running": "limegreen",
        "color:paused": "lightskyblue",
        "color:stopped": "white"
    },
    "key-combos": {
        "startOrPause: "CommandOrControl+`",
        "reset": "CommandOrControl+Alt+`" 
    }
}
```

### Key combinations
In the above example, this will set the timer to have a blue background, and the numbers will be red while the timer is running, green when it's paused, and white when it is stopped. This also sets the start/pause combination to Ctrl+\` (Cmd+\` if on Mac), and the reset to Ctrl+Alt+\` (Cmd+Alt+\` if on Mac). A full writeup on key combinations can be found here: [Electron Key Codes](https://www.electronjs.org/docs/api/accelerator#available-modifiers). 

### Styling 
To tie a styling to a specific state (running, paused, stopped), you must format it like such:  
`"<format>:<state>": "<value>"`.  
If you only specify the format, it will by default only affect the area around the text. 

So, for example, if you want to change the color to red, and the font size to 20, and give it a shadow when the timer is the "running" state, you would do something like:

```
{
    "styles": {
        "background-color": "magenta",
        "color:running": "red",                   <--
        "font-size:running": "20px",              <--
        "text-shadow:running": "2px 2px black",   <--
        "color:paused": "lightskyblue",
        "color:stopped": "white"
    },
    "key-combos": {
        "startOrPause": "CommandOrControl+`",
        "reset": "CommandOrControl+Alt+`"
    }
}
```
Where the lines with arrows pointing to them pertain to the changes mentioned above. For more information regarding CSS styles you can check out this: [Mozilla Developer Docs](https://developer.mozilla.org/en-US/docs/Web/CSS/Reference), [W3 Schools](https://www.w3schools.com/cssref/). If you only want to change simple things like the colors, just simply modify the existing config properties.
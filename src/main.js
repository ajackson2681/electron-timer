const {
    app,
    BrowserWindow,
    globalShortcut
} = require('electron');
const fs = require("fs");
const { homedir } = require('os');
const path = require("path");

const appPath = app.getAppPath();
const appDir = appPath.endsWith(".asar") ? appPath.split("/").slice(0,-1).join("/") : appPath;

const homeDir = process.env.HOME;

function createWindow() {

    // Create the browser window.
    let win = new BrowserWindow({
        width: 500,
        height: 85,
        autoHideMenuBar: true,
        resizable: false,
        webPreferences: {
            nodeIntegration: true,
            contextIsolation: false
        }
    });
    
    win.setMenu(null);

    win.loadFile('src/timer.html')

    win.on('closed', () => {
        win = null;
    });

    let keyCombos;
    
    try {
        keyCombos = config["key-combos"];
    }
    catch {
        keyCombos = {
            "startOrPause": "CommandOrControl+`",
            "reset": "CommandOrControl+Alt+`"
        };
    }

    // allows us to start the timer without the window being focused.
    // handled by ipcRenderer.on('startOrPause') in split.js
    globalShortcut.register(keyCombos["startOrPause"], () => {
        win.webContents.send('startOrPause');
    });

    // allows us to reset the timer without the window being focused.
    // handled by ipcRenderer.on('reset') in split.js
    globalShortcut.register(keyCombos["reset"], () => {
        win.webContents.send('reset');
    });
}

app.on('ready', createWindow);

app.on('window-all-closed', () => {
    app.quit()
});

// creates the .config directory if it doesn't exist (likely only on windows platforms)
if (!fs.existsSync(path.join(`${homedir}`,".config"))) {
    fs.mkdirSync(path.join(`${homeDir}`,".config"));
}

// checks to ensure the config file exists, and if it doesn't creates a default
if (!fs.existsSync(path.join(`${homeDir}`,".config","electron-timer.json"))) {
    fs.copyFileSync(
        path.join(`${appDir}`,"utils","config.template.json"),
        path.join(`${homeDir}`,".config","electron-timer.json")
    );
}
const { ipcRenderer } = require("electron");
const fs = require("fs");
const path = require("path");

// this is a handle for the setInterval. It is cleared and nulled
// when the timer is stopped so we can determine if the timer is
// only meant to be paused or reset.
let intervalHandle = null;

// holds the Date.now() value when the timer is started. Allows
// us to determine the amount of time elapsed since the start
let startTime = null;

// represents the html element that holds the timer
let timer = null;

// these allow us to just simply set the classList without
// having to "add" and "remove" classes
let running = ["running"];
let paused = ["paused"];
let stopped = ["stopped"];

// global vars pertaining to the timer state
let pauseAdjust = 0;
let diff = 0;
let now;

// listener for when the main process starts or pauses the timer
ipcRenderer.on('startOrPause', () => {
    if (!intervalHandle) { // if the intervalHandle is null, it means no timer is running
        startTimer();
    }
    else { // if it is not null, then it _is_ running and should be paused.
        pauseTimer();
    }
});

// listener for when the main process resets the timer
ipcRenderer.on('reset', () => {
    resetTimer();
});

/**
 * This function simply pauses the timer, it does _not_ reset it.
 * The startTime does not get nulled, and the timer retains
 * its styling.
 */
function pauseTimer() {
    clearInterval(intervalHandle);
    pauseAdjust = diff;
    timer.classList = paused;
    intervalHandle = null;
}

/**
 * Starts the timer, if the timer has already been started, resumes it.
 * If the timer is stopped, starts a new one.
 */
function startTimer() {
    startTime = Date.now();

    intervalHandle = setInterval(() => {
        now = Date.now();
        if (pauseAdjust) {
            diff = (now - startTime) + pauseAdjust;
        }
        else {
            diff = now - startTime;
        }
         
        const seconds = Math.floor((diff/1000) % 60);
        const minutes = Math.floor((diff/1000/60) % 60);
        const hours = Math.floor(diff/(1000*60*60));

        timer.innerHTML = 
            `${hours}:`+
            `${('00' + minutes).slice(-2)}:`+
            `${('00' + seconds).slice(-2)}.`+
            `${('000' + (diff%1000)).slice(-3)}`;
    }, 1);

    timer.classList = running;
}

/**
 * Resets the timer, this _does_ change the styling on the timer,
 * and also nulls startTime.
 */
function resetTimer() {
    pauseTimer();
    pauseAdjust = 0;
    timer.innerHTML = `0:00:00.000`;
    timer.classList = stopped;
    startTime = null;
}


/**
 * This parses the object loaded from the config file and determines whether
 * it should affect the background or the number font. Any keys that contain
 * a ':' are assumed to be affecting the font.
 * 
 * @param {object} config The config file object loaded from *.json
 */
function styleConfig(config) {
    if (config) {
        Object.keys(config).forEach( (key) => {
            if (key.includes(":")) {
                styleFont(key, config[key]);
            } 
            else {
                timer.style[key] = config[key];
            }
        });
    }
}

/**
 * Parses the key/value pair from the styles object from the config file
 * and applies it to the timer font.
 * 
 * @param {string} key A key that speicifies the CSS property and the 
 * CSS class (timer state) to affect, separated by a ':'
 * @param {string} property The value to assign to the CSS property
 */
function styleFont(key, property) {
    const splitVals = key.split(":");
    const newStyle = document.createElement("style");
    document.head.appendChild(newStyle);
    const sheet = newStyle.sheet;
    sheet.insertRule(`.${splitVals[1]} { ${splitVals[0]}: ${property}}`);
}

/**
 * Sets initial styling and loads timer by id.
 */
window.onload = () => {
    timer = document.getElementById("timer");
    timer.innerHTML = `0:00:00.000`;

    // determines the home directory based on OS
    const homeDir = process.env.HOME;

    try {
        const config = require(path.join(`${homeDir}`,".config","electron-timer.json"))["styles"];
        styleConfig(config);
    }
    catch(e) {/** do nothing */}
}
#!/bin/sh

if [ "$(id -u)" -ne 0 ]
then
    echo "This script requires root permissions to run. Rerun this script by using sudo $0."
    exit 1
fi

#copies the library to the /lib directory
cp -r electron-timer-linux-x64 /lib

# creates a symlink in the /usr/bin directory so that it can be executed on the command line
ln -s /lib/electron-timer-linux-x64/electron-timer /usr/bin/electron-timer